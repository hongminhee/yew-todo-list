use std::collections::HashMap;

use gloo_storage::{LocalStorage, Storage};
use todo_list::{TodoItem, TodoList};
use uuid::Uuid;
use yew::{function_component, html, Callback};

#[function_component(App)]
fn app() -> Html {
    let saved_map: HashMap<Uuid, TodoItem> =
        LocalStorage::get_all().unwrap_or_default();
    log::debug!("saved_map: {:?}", saved_map);
    let initial_items =
        Vec::from_iter(saved_map.values().map(|i| i.to_owned()));
    let on_set = Callback::from(|item: TodoItem| {
        let key = item.id.to_simple().to_string();
        LocalStorage::set(key, item).unwrap();
    });
    let on_remove = Callback::from(|item_id: Uuid| {
        let key = item_id.to_simple().to_string();
        LocalStorage::delete(key);
    });
    let style = stylist::style!(
        r#"
        margin: 0 auto; max-width: 40rem; padding: 1rem 0 0 0;
        background: #111;
        cursor: default;
        & > * { margin: 1rem 0; padding: 0 1rem; }
        & > :first-child { margin: 0; }
        "#
    )
    .expect("Failed to mount style");
    html! {
        <main class={style.get_class_name().to_owned()}>
            <h1>{"Todo List in Yew"}</h1>
            <TodoList
                {initial_items}
                on_set={Some(on_set)}
                on_remove={Some(on_remove)} />
        </main>
    }
}

fn main() {
    wasm_logger::init(wasm_logger::Config::default());
    yew::start_app::<App>();
}
