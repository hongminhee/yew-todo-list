use crate::{TodoInput, TodoItem, TodoListFilter};
use stylist::style;
use uuid::Uuid;
use yew::{html, Callback, Component, Context, Html, Properties};

type ShouldRerender = bool;

#[derive(Clone, Debug)]
pub enum TodoListMsg {
    Add(String),
    Remove(Uuid),
    Toggle(Uuid),
    Filter(TodoListFilter),
}

#[derive(Properties, PartialEq)]
pub struct TodoListProps {
    pub initial_items: Vec<TodoItem>,
    pub on_set: Option<Callback<TodoItem>>,
    pub on_remove: Option<Callback<Uuid>>,
}

#[derive(Clone, Debug)]
pub struct TodoList {
    filter: TodoListFilter,
    items: Vec<TodoItem>,
}

impl TodoList {
    fn count_remaining(&self) -> usize {
        self.items.iter().filter(|item| !item.done).count()
    }
}

impl Component for TodoList {
    type Message = TodoListMsg;
    type Properties = TodoListProps;

    fn create(ctx: &Context<Self>) -> Self {
        Self {
            filter: TodoListFilter::All,
            items: ctx.props().initial_items.clone(),
        }
    }

    fn update(
        &mut self,
        ctx: &Context<Self>,
        msg: Self::Message,
    ) -> ShouldRerender {
        match msg {
            TodoListMsg::Add(text) => {
                let item = TodoItem {
                    id: Uuid::new_v4(),
                    text,
                    done: false,
                };
                if let Some(cb) = &ctx.props().on_set {
                    cb.clone().emit(item.clone());
                }
                self.items.push(item);
                true
            }
            TodoListMsg::Remove(id) => {
                let mut index = None;
                for (i, item) in self.items.iter().enumerate() {
                    if item.id == id {
                        index = Some(i);
                        break;
                    }
                }
                if let Some(index) = index {
                    let item = self.items.remove(index);
                    if let Some(cb) = &ctx.props().on_remove {
                        cb.clone().emit(item.id);
                    }
                    true
                } else {
                    false
                }
            }
            TodoListMsg::Toggle(id) => {
                for item in self.items.iter_mut() {
                    if item.id == id {
                        item.done = !item.done;
                        if let Some(cb) = &ctx.props().on_set {
                            cb.clone().emit(item.clone());
                        }
                        return true;
                    }
                }
                false
            }
            TodoListMsg::Filter(filter) => {
                self.filter = filter;
                true
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link();
        let remaining = self.count_remaining();
        let on_complete = link.callback(|text| {
            log::debug!("text on complete: {:?}", text);
            TodoListMsg::Add(text)
        });
        let items = self
            .items
            .clone()
            .into_iter()
            .filter(|item| self.filter.filter(item))
            .map(|item| {
                let on_toggle = link.callback(TodoListMsg::Toggle);
                let on_remove = link.callback(TodoListMsg::Remove);
                let key: i128 =
                    i128::from_le_bytes(item.id.as_bytes().to_owned());
                html! {
                    <TodoItem {key} {item} {on_toggle} {on_remove} />
                }
            });
        let style = style!(
            r#"
            margin: 0; padding: 0; list-style: none;
            "#
        )
        .expect("Failed to mount style");
        html! {
            <>
                <p>{format!("{} items are left", remaining)}</p>
                <TodoInput
                    initial_value={""}
                    placeholder={"Type an item to add"}
                    button_text={"Add"}
                    {on_complete}
                    />
                <ul class={style.get_class_name().to_owned()}>{for items}</ul>
                <TodoListFilter on_filter={link.callback(TodoListMsg::Filter)} />
            </>
        }
    }
}
