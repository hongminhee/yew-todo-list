use serde::{Deserialize, Serialize};
use stylist::style;
use uuid::Uuid;
use yew::{html, Callback, Component, Context, Html, Properties};

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct TodoItem {
    pub id: Uuid,
    pub text: String,
    pub done: bool,
}

impl TodoItem {
    pub fn toggle(&mut self) {
        self.done = !self.done;
    }
}

#[derive(Properties, PartialEq)]
pub struct TodoItemProps {
    pub item: TodoItem,
    pub on_toggle: Callback<Uuid>,
    pub on_remove: Callback<Uuid>,
}

impl Component for TodoItem {
    type Message = ();
    type Properties = TodoItemProps;

    fn create(ctx: &Context<Self>) -> Self {
        ctx.props().item.clone()
    }

    fn update(&mut self, _ctx: &Context<Self>, _msg: Self::Message) -> bool {
        self.done = !self.done;
        true
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link();
        let on_toggle_click = {
            let item_id = self.id;
            let on_toggle = ctx.props().on_toggle.clone();
            let send_msg = link.callback(|_| ());
            Callback::from(move |_| {
                on_toggle.emit(item_id);
                send_msg.emit(item_id);
            })
        };
        let on_remove_click = {
            let item_id = self.id;
            let on_remove = ctx.props().on_remove.clone();
            Callback::from(move |_| {
                on_remove.emit(item_id);
            })
        };
        let style = style!(
            r#"
            margin: 0; padding: 1rem;
            border-top: 1px solid #222;
            font-size: x-large;

            &:last-child { border-bottom: 1px solid #222; }
            &:hover { background: #090909; }
            &.done { text-decoration: line-through; color: #999; }

            button {
                background: transparent;
                border: none;
                font-size: inherit;
            }
            button.toggle { color: gray; }
            button.remove { float: right; }
            "#
        )
        .expect("Failed to mount style");
        let class = format!(
            "{} {}",
            style.get_class_name(),
            if self.done { "done" } else { "" }
        );
        html! {
            <li {class}>
                <button onclick={on_toggle_click} class="toggle">{
                    if self.done { "☑" } else { "☐" }
                }</button>
                <span>{self.text.clone()}</span>
                <button onclick={on_remove_click} class="remove">{"🗑"}</button>
            </li>
        }
    }
}
