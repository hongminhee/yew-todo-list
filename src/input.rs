use stylist::style;
use wasm_bindgen::JsCast;
use web_sys::{Event, FocusEvent, HtmlInputElement};
use yew::{function_component, html, use_state, Callback, Properties};

#[derive(Properties, PartialEq)]
pub struct TodoInputProps {
    pub initial_value: String,
    pub placeholder: String,
    pub button_text: String,
    pub on_complete: Callback<String>,
}

#[function_component(TodoInput)]
pub fn todo_input(props: &TodoInputProps) -> Html {
    let initial_value = props.initial_value.to_owned();
    let input_value = use_state(|| initial_value);
    log::debug!("input_value: {:?}", input_value);
    let value_setter = input_value.setter();
    let value = (*input_value).clone();
    let placeholder = props.placeholder.to_owned();
    let button_text = props.button_text.to_owned();
    let on_complete = props.on_complete.to_owned();
    let onsubmit = Callback::from(move |e: FocusEvent| {
        e.prevent_default();
        log::debug!("input_value on submit: {:?}", input_value);
        if !input_value.is_empty() {
            on_complete.emit((*input_value).to_owned());
            input_value.set(String::new());
        }
    });
    let onchange = Callback::from(move |e: Event| {
        let target = e
            .target()
            .expect("Event should have a target when dispatched.");
        value_setter.set(target.unchecked_into::<HtmlInputElement>().value());
    });
    let style = style!(
        r#"
        input, button { font-size: x-large; }
        button { cursor: pointer; }
        "#
    )
    .expect("Failed to mount style");
    html! {
        <form class={style.get_class_name().to_owned()} {onsubmit}>
            <input type="text" {value} {placeholder} {onchange} />
            <button type="submit">{button_text}</button>
        </form>
    }
}
