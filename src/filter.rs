use std::fmt::{Display, Error, Formatter};

use stylist::style;
use yew::{html, Callback, Component, Context, Html, Properties};

use crate::TodoItem;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum TodoListFilter {
    All,
    Active,
    Completed,
}

impl TodoListFilter {
    pub fn available_filters() -> Vec<TodoListFilter> {
        vec![
            TodoListFilter::All,
            TodoListFilter::Active,
            TodoListFilter::Completed,
        ]
    }

    pub fn filter(&self, todo: &TodoItem) -> bool {
        match self {
            TodoListFilter::All => true,
            TodoListFilter::Active => !todo.done,
            TodoListFilter::Completed => todo.done,
        }
    }
}

impl Display for TodoListFilter {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        match self {
            TodoListFilter::All => write!(f, "All"),
            TodoListFilter::Active => write!(f, "Active"),
            TodoListFilter::Completed => write!(f, "Completed"),
        }
    }
}

#[derive(Properties, PartialEq)]
pub struct TodoListFilterProps {
    pub on_filter: Callback<TodoListFilter>,
}

impl Component for TodoListFilter {
    type Message = Self;
    type Properties = TodoListFilterProps;

    fn create(_ctx: &Context<Self>) -> Self {
        Self::All
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        let changed = *self != msg;
        *self = msg;
        changed
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let on_filter = ctx.props().on_filter.to_owned();
        let filters = Self::available_filters()
            .into_iter()
            .map(|filter| {
                let active = filter == *self;
                let f = on_filter.to_owned();
                let switch_tab = ctx.link().callback(|f| f);
                let onclick = Callback::from(move |_| {
                    switch_tab.emit(filter);
                    f.emit(filter);
                });
                html! {
                    <a
                        {onclick}
                        class={if active { "selected" } else { "" }}>
                        {format!("{}", filter)}
                    </a>
                }
            })
            .collect::<Html>();
        let style = style!(
            r#"
            display: flex; align-items: stretch;
            margin: 0; padding: 0;
            font-size: x-large; font-family: sans-serif;
            text-transform: uppercase;

            a {
                flex: 1; display: block;
                padding: 1rem;
                border-right: 1px solid #222; color: #999;
                text-align: center;
                cursor: pointer;
            }
            a:hover { background: #090909; }
            a.selected { background: #090909; font-weight: bolder; }
            "#
        )
        .expect("failed to mount style");
        html! {
            <div class={style.get_class_name().to_owned()}>{filters}</div>
        }
    }
}
