Todo List built within Yew
==========================

This sample app implements a todo list within Yew, a React-like
web framework based on WASM for Rust.  Here's a published app:

<https://hongminhee.gitlab.io/yew-todo-list>

To build it by yourself:

~~~~ bash
cargo install trunk
rustup target add wasm32-unknown-unknown
cargo install trunk wasm-bindgen-cli 
trunk serve
~~~~

It took about 13 hours to implement everything here.  It had to learn
the below things during making this app:

 -  Yew: How to deal with `<input>` value as component state
 -  Yew: `JsCast` for event targets
 -  Yew: Function components & hooks
 -  Yew: Logging using *wasm-logger* crate
 -  gloo-storage: Persist data using `sessionStorage`
 -  stylist-rs: Styled components
 -  Rust: Some traits like `Display`
 -  Rust: Mutating vector elements during iteration using `.iter_mut()`
 -  Rust: re-exporting modules
 -  CSS: `flex` property
 -  GitLab CI

Although it's trivial, it's distributed under MIT license.